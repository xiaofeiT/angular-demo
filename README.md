﻿# Angular-demo|angular练手项目

## 框架使用

### UI 使用 layui
### 前段框架 Angular

## 简介

进行商品库存的管理   
未登录情况下，能查看当前库存，更改每个商品的库存数量  
登录之后，可以增加删除商品，可以更改管理员个人信息  
页面使用Layui中的一些动画  

## RUN
1. 进入src目录（与docs同级的）。
2. 使用npm install补上依赖包。也可以把之前备份的node_modules目录复制一份在项目根目录下。
3. 使用json-server data.json启动后端服务。
4. 使用npm start运行项目，打开浏览器访问 http://localhost:4200/ 进入项目。  

## TODO

- 适配移动端
- 优化页面动画
- 页面重构
- 完善逻辑